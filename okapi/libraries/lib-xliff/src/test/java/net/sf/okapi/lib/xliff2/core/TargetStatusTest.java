package net.sf.okapi.lib.xliff2.core;

import static org.junit.Assert.assertEquals;

import net.sf.okapi.lib.xliff2.core.TargetState;

import org.junit.Test;

public class TargetStatusTest {

	@Test
	public void testSimple () {
		assertEquals("initial", TargetState.INITIAL.toString());
		assertEquals("translated", TargetState.TRANSLATED.toString());
		assertEquals("reviewed", TargetState.REVIEWED.toString());
		assertEquals("final", TargetState.FINAL.toString());
	}
	
	@Test
	public void testCompare () {
		assertEquals(true, TargetState.INITIAL.compareTo(TargetState.TRANSLATED)<0);
		assertEquals(true, TargetState.INITIAL.compareTo(TargetState.REVIEWED)<0);
		assertEquals(true, TargetState.TRANSLATED.compareTo(TargetState.REVIEWED)<0);
		assertEquals(true, TargetState.REVIEWED.compareTo(TargetState.FINAL)<0);
		assertEquals(true, TargetState.INITIAL.compareTo(TargetState.FINAL)<0);
		assertEquals(true, TargetState.TRANSLATED.compareTo(TargetState.FINAL)<0);
	}

	@Test
	public void testToFromString () {
		assertEquals(TargetState.INITIAL, TargetState.fromString(TargetState.INITIAL.toString()));
		assertEquals(TargetState.TRANSLATED, TargetState.fromString(TargetState.TRANSLATED.toString()));
		assertEquals(TargetState.REVIEWED, TargetState.fromString(TargetState.REVIEWED.toString()));
		assertEquals(TargetState.FINAL, TargetState.fromString(TargetState.FINAL.toString()));
	}

}
